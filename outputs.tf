output "vpc_id" {
  description = "Id of the created vpc."
  value       = aws_vpc.this.id
}

output "vpc_arn" {
  description = "Arn of the created vpc."
  value       = aws_vpc.this.arn
}

output "subnets_ids" {
  description = "Ids of the created subnets of VPC."
  value = [
    for subnet in aws_subnet.subnet : subnet.id
  ]
}

output "subnets_arns" {
  description = "Arns of the created subnets of VPC."
  value = [
    for subnet in aws_subnet.subnet : subnet.arn
  ]
}



output "debug" {
  description = "For debug purpose."
  value       = local.vpc
}
