locals {
  vpc     = var.resources
  tags    = try(local.vpc.tags != null ? local.vpc.tags : null, null)
  subnets = local.vpc.subnets

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_vpc" "this" {
  cidr_block = local.vpc.cidr_block

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.vpc.name
    Description = "Created by ${var.project.createdBy}"
  })
}

resource "aws_subnet" "subnet" {
  for_each   = { for subnet in local.subnets : subnet.name => subnet }
  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block

  tags = merge(local.common_tags, try(each.value.tags != null ? each.value.tags : {
    Name        = each.value.name
    Description = "Created by ${var.project.createdBy}"
  }, []))
}
